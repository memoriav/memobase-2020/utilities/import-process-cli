#!/usr/bin/env bash


for d in baz-001 cic-001 mfk-001;
do
  ssh root@mb-wf2.memobase.unibas.ch "mkdir -p /swissbib_index/mb_sftp/test/$d/config"
  scp test/$d/mappings/localTransforms.yml root@mb-wf2.memobase.unibas.ch:/swissbib_index/mb_sftp/test/$d/config/localTransforms.yml;
  scp test/$d/mappings/mapping.yml root@mb-wf2.memobase.unibas.ch:/swissbib_index/mb_sftp/test/$d/config/mapping.yml;
  scp test/$d/mappings/transform.xslt root@mb-wf2.memobase.unibas.ch:/swissbib_index/mb_sftp/test/$d/config/transform.xslt;
  echo "Copied $d/mappings to $d/config"
  sleep 10;
done

ssh root@mb-wf2.memobase.unibas.ch "chown -R mb_sftp:mb_sftp /swissbib_index/mb_sftp/test"