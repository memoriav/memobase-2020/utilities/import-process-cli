import json
import logging
import os
import sys

from api.run import WorkflowRunner

API_ENDPOINT = 'https://stage.import.memobase.k8s.unibas.ch/v1'

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

if __name__ == '__main__':
    for directory in os.listdir('stage/'):
        if os.path.isdir("stage/" + directory):
            with open(f'stage/{directory}/setup.json', 'r') as fp:
                workflow = WorkflowRunner(API_ENDPOINT, **json.load(fp))
                res = workflow.run()
                print('Endpoint: {}\nStatus: {}\nResponse: {}'.format(
                    res[0], res[1], res[2]))
