import json
import logging
import os
import sys

from api.run import WorkflowRunner

API_ENDPOINT = 'https://test.import.memobase.k8s.unibas.ch/v1'

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

if __name__ == '__main__':
    for directory in os.listdir('test/'):
        if os.path.isdir("test/" + directory):
            with open(f'test/{directory}/setup.json', 'r') as fp:
                workflow = WorkflowRunner(API_ENDPOINT, **json.load(fp))
                res = workflow.run()
                print('Endpoint: {}\nStatus: {}\nResponse: {}'.format(
                    res[0], res[1], res[2]))
