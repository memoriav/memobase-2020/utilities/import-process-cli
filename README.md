### Import Process CLI
These files are used to run the migration of the memobase data.

The configurations for the prod and stage environments are maintained [here](https://gitlab.switch.ch/memoriav/memobase-2020/configurations/import-process).

## Files & Folders
To run these scripts a python installation is required with the package `requests`. The suffix of
the script determines on which infrastructure the request will be executed.

```bash
python start-prod.py record-set-memobase-id
```
To start a full migration use:
```bash
python migration-start-prod.py
```

### Prod
A list of all the import process settings  used in the import processes for the production service.
### Stage
A list of all the import process settings used in the import processes for the stage service.
### Test
A list of all the configurations used in the import processes for the test service.