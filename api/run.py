import requests
import uuid


class WorkflowRunner:

    def __init__(self, api_endpoint, **kwargs):
        self.api_endpoint = api_endpoint
        self.institution_id = kwargs['institutionId']
        self.record_set_id = kwargs['recordSetId']
        self.xml_identifier_field_name = kwargs['identifierFieldName'] if 'identifierFieldName' in kwargs else 'none'
        self.xml_record_tag = kwargs['recordTag'] if 'recordTag' in kwargs else 'none'
        self.table_sheet_index = kwargs['tableSheetIndex'] if 'tableSheetIndex' in kwargs else 1
        self.table_header_count = kwargs['tableHeaderCount'] if 'tableHeaderCount' in kwargs else 1
        self.table_header_index = kwargs['tableHeaderIndex'] if 'tableHeaderIndex' in kwargs else 1
        self.table_identifier_index = kwargs['tableIdentifierIndex'] if 'tableIdentifierIndex' in kwargs else 1

    def run(self):
        drupal_uuid = str(uuid.uuid4())
        session_id = str(uuid.uuid4())
        data = {
            'job-parameters': {
                'drupalJobUuid': drupal_uuid,
                'drupalJobLogResultUuid': drupal_uuid,
                'recordSetId': self.record_set_id,
                'sessionId': session_id,
                'institutionId': self.institution_id,
                'xmlIdentifierFieldName': self.xml_identifier_field_name,
                'xmlRecordTag': self.xml_record_tag,
                'isPublished': True,
                'tableSheetIndex': self.table_sheet_index,
                'tableHeaderCount': self.table_header_count,
                'tableHeaderIndex': self.table_header_index,
                'tableIdentifierIndex': self.table_identifier_index,
            }
        }
        url_workflow = '{}/importprocess/{}/{}/start'.format(self.api_endpoint, self.institution_id, self.record_set_id)
        res_workflow = requests.post(url=url_workflow, json=data, headers={'Accept': 'application/json'})
        if res_workflow.ok:
            return url_workflow, res_workflow.status_code, res_workflow.json()
        else:
            return url_workflow, res_workflow.status_code, res_workflow.text