import json
from api.run import WorkflowRunner
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

if __name__ == '__main__':

    API_ENDPOINT = 'https://test.import.memobase.k8s.unibas.ch/v1'
    with open(f'test/{sys.argv[1]}/setup.json', 'r') as fp:
        workflow = WorkflowRunner(API_ENDPOINT, **json.load(fp))
        res = workflow.run()
        print('Endpoint: {}\nStatus: {}\nResponse: {}'.format(
            res[0], res[1], res[2]))
