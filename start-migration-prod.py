import json
import logging
import os
import sys

from api.run import WorkflowRunner

API_ENDPOINT = 'https://import.memobase.k8s.unibas.ch/v1'

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

if __name__ == '__main__':
    for directory in os.listdir('prod/'):
        if os.path.isdir("prod/" + directory):
            with open(f'prod/{directory}/setup.json', 'r') as fp:
                workflow = WorkflowRunner(API_ENDPOINT, **json.load(fp))
                res = workflow.run()
                print('Endpoint: {}\nStatus: {}\nResponse: {}'.format(
                    res[0], res[1], res[2]))
